@servers(['local' => '127.0.0.1', 'staging' => 'root@10.1.246.40', 'production' => ['root@10.1.246.40']])

@setup

    // the repository to clone
    $repo = 'https://bitbucket.org/Saffah/wordpress/src/master/';

    // the branch to clone
    $branch = 'master';

    // set up timezones
    date_default_timezone_set('Europe/Berlin');
    
    // we want the releases to be timestamps to ensure uniqueness
    $date = date('YmdHis');

    // the application directory on your server
    $appDir = '~/sites/k.d.dk';

    // this is where the releases will be stored
    $buildsDir = $appDir . '/releases';

    // this is where the deployment will be
    $deploymentDir = $buildsDir . '/' . $date;

    // and this is the document root directory
    $serve = $appDir . '/www/html';
    
@endsetup

@task('dir')
    echo "Preparing new deployment directory..."
    
    cd {{ $buildsDir }}
    mkdir {{ $date }}
    
    echo "Preparing new deployment directory complete."
@endtask

@task('git')
    echo "Cloning repository..."
    
    cd {{ $deploymentDir }}
    git clone --depth 1 -b {{ $branch }} "{{ $repo }}" {{ $deploymentDir }}

    echo "Cloning repository complete."
@endtask

@task('install')
    echo "Installing dependencies...";

    composer install --prefer-dist
    
    echo "Installing dependencies complete."
@endtask

@task('live')
    echo "Creating symlinks for the live version..."
    
    cd {{ $deploymentDir }}
    ln -nfs {{ $deploymentDir }} {{ $serve }}
    ln -nfs {{ $appDir }}/uploads {{ $serve }}/wp-content/
    
    echo "Creating symlinks completed."
@endtask

@task('deployment_cleanup')
    echo "Cleaning up old deployments..."
	
    cd {{ $buildsDir }}
    ls -t | tail -n +4 | xargs rm -rf
    
	echo "Cleaned up old deployments."
@endtask

@story('deploy-staging', ['on' => 'staging'])
    dir
    git
    install
    live
    deployment_cleanup
@endstory

@story('deploy-production', ['on' => 'production'])
    dir
    git
    install
    live
    deployment_cleanup
@endstory

