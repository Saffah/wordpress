<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'johnpbloch/wordpress' => 
    array (
      'pretty_version' => '5.5.1',
      'version' => '5.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0973abd5c01ecce0b60bc1c81a6dd072827de930',
    ),
    'johnpbloch/wordpress-core' => 
    array (
      'pretty_version' => '5.5.1',
      'version' => '5.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa5bcb1579eae33baef6c04355f8d6657e5bd349',
    ),
    'johnpbloch/wordpress-core-installer' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '237faae9a60a4a2e1d45dce1a5836ffa616de63e',
    ),
    'wordpress/core-implementation' => 
    array (
      'provided' => 
      array (
        0 => '5.5.1',
      ),
    ),
  ),
);
